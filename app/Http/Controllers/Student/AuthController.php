<?php

namespace App\Http\Controllers\Student;

use App\Http\Requests\StudentRegister;
use App\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{


    public function studentRegister(StudentRegister $request)
    {


        $newUser = Student::create($request->all());
        $tokenStr = $newUser->createToken('Token Name')->accessToken;
        return response()->json($tokenStr, 200);

    }


}
