<?php

namespace App\Http\Controllers\Bus;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Bus;
use JWTAuth;
use JWTAuthException;
use Config;




class AuthController extends Controller
{


    public function busRegister(StudentRegister $request)
    {


        $newUser = Bus::create($request->all());
        $tokenStr = $newUser->createToken('Token Name')->accessToken;
        return response()->json($tokenStr, 200);

    }

}
