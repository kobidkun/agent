<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\User;
//use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;
use Auth;
class AuthController extends Controller {

    public function authenticate(\Illuminate\Http\Request $request, JWTAuth $auth) {
        $credentials = $request->only('email', 'password'); // grab credentials from the request
        try {
            if (!$token = \Tymon\JWTAuth\Facades\JWTAuth::attempt($credentials)) { // attempt to verify the credentials and create a token for the user
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500); // something went wrong whilst attempting to encode the token
        }

       // $auth->$token;

        //$s = \Tymon\JWTAuth\JWTAuth::toUser();
     //   $auth->parseToken();
        $user = $currentUser = Auth::user();;


        return response()->json(['token' => "$token", 'user' => $user]);
    }

    public function userdata(){
        $a = User::all();
        return response()->json(['user' => $a], 200);
    }


    public function RegisterBusUser(Request $request){
         $s = new User();
        $s->name = $request->name;
        $s->email = $request->email;
        $s->password = bcrypt($request->password);
        $s->save();
        return response()->json('new user updated', 200);
    }


}
?>