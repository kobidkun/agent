<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sender_id')->unasigned();
            $table->integer('receiver_id')->nullable();
            $table->integer('bus_id')->unasigned()->nullable();
            $table->text('subject')->nullable();
            $table->text('message')->nullable();
            $table->text('latitude')->nullable();
            $table->text('longitude')->nullable();
            $table->text('address')->nullable();
            $table->text('speed')->nullable();
            $table->boolean('read')->nullable();
            $table->boolean('is_moving')->default(false);

            $table->timestamps();

            $table->index('bus_id');
            $table->index('sender_id');
            $table->index('receiver_id');
            $table->index(['receiver_id','read']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
